const path = require('path');
const nativeImage = require('electron').nativeImage;
const { app, Menu, Tray, Notification } = require('electron')
const { echidnalink } = require('echidnalink-lib');

let tray = null;

app.on('ready', () => {
    const iconPath = path.join(__dirname, 'images/echidna_40_24.png');
    const trayIcon = nativeImage.createFromPath(iconPath);

    tray = new Tray(trayIcon);

    const contextMenu = Menu.buildFromTemplate([
        { label: 'EchidnaLink v1.3.0' },
        { type: 'separator' },
        { label: 'Salir', click() { app.quit() } },
    ])
    
    echidnalink.startLink(tray).then(elink => {
        elink.board.on('fail', (e) => {
            console.log("Error al iniciar el link, posiblemente la placa no esté conectada: " + e.message);
         });
     
         tray.setToolTip('EchidnaLink activo');
         tray.setContextMenu(contextMenu);
    });

})