SET VERSION=v16.0.4
SET PLATFORM=win32
SET VERSION_ECHIDNALINK=1.3.0

REM Descago todas las dependencias de node.js y reconstruyo los módulo nativos de
REM Node.js contra la version de Node.js que el proyecto Electron está usando. 
REM This allows you to use native Node.js modules in Electron apps without your 
REM system version of Node.js matching exactly (which is often not the case, and 
REM sometimes not even possible). 

cd .. 
cmd /C npm install
cmd /C node_modules\.bin\electron-rebuild
cd build_tools

REM Borro version anterior
del /S /Q electron-%VERSION%-%PLATFORM%-x64

REM Creo paquete asar
del /S /Q app
mkdir app
copy ..\package.json app
copy ..\main.js app
copy ..\index.html app
copy ..\echidnalink.js app
copy ..\EchidnaBoard.js app
xcopy /s ..\node_modules app\node_modules\
xcopy /s ..\images app\images\

cmd /C ..\node_modules\.bin\asar pack app app.asar

REM Creo directorio para alojar la distribución
mkdir electron-%VERSION%-%PLATFORM%-x64

REM Copio el paquete precompilado de electron y lo descomprimo
copy electron-prebuilt\electron-%VERSION%-%PLATFORM%-x64.zip .
powershell -executionpolicy unrestricted -command "Expand-Archive electron-%VERSION%-%PLATFORM%-x64.zip"
del  /Q .\electron-%VERSION%-%PLATFORM%-x64.zip

REM cambio el nombre del directorio electron por echidna
move electron-%VERSION%-%PLATFORM%-x64 echidnalink-%VERSION_ECHIDNALINK%-windows-x64

REM Llevo el paquete asar a la carpeta de electron
move app.asar echidnalink-%VERSION_ECHIDNALINK%-windows-x64\resources\app.asar

REM Cambio nombre de la aplicación
move echidnalink-%VERSION_ECHIDNALINK%-windows-x64\electron.exe echidnalink-%VERSION_ECHIDNALINK%-windows-x64\echidnalink.exe

REM Cambio icono de la aplicación
cmd /C rcedit-x64.exe echidnalink-%VERSION_ECHIDNALINK%-windows-x64\echidnalink.exe --set-icon app\images\echidna.ico
