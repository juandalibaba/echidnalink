VERSION=v16.0.4
PLATFORM=darwin
VERSION_ECHIDNALINK=1.3.0

# Descago todas las dependencias de node.js y reconstruyo los módulo nativos de
# Node.js contra la version de Node.js que el proyecto Electron está usando. 
# This allows you to use native Node.js modules in Electron apps without your 
# system version of Node.js matching exactly (which is often not the case, and 
# sometimes not even possible). 

cd .. 
npm install
./node_modules/.bin/electron-rebuild
cd build_tools

# Borro version anterior
rm -rf echidna-$VERSION_ECHIDNALINK-$PLATFORM-x64

# Creo paquete asar
rm -rf app
mkdir app
cp ../package.json app/
cp ../main.js app/
cp ../index.html app/
cp ../echidnalink.js app/
cp ../EchidnaBoard.js app/
cp -r ../node_modules app/
cp -r ../images app

../node_modules/.bin/asar pack app app.asar

# Creo directorio para alojar la distribución
mkdir echidna-$VERSION_ECHIDNALINK-$PLATFORM-x64

# Copio el paquete precompilado de electron y lo descomprimo
cp electron-prebuilt/electron-$VERSION-$PLATFORM-x64.zip echidna-$VERSION_ECHIDNALINK-$PLATFORM-x64
cd echidna-$VERSION_ECHIDNALINK-$PLATFORM-x64
unzip electron-$VERSION-$PLATFORM-x64.zip
rm electron-$VERSION-$PLATFORM-x64.zip 

# Llevo el paquete asar a la carpeta de electron
mv ../app.asar Electron.app/Contents/Resources/app.asar

# Cambio nombre de la aplicació
mv Electron.app Echidnalink.app

# Cambio metadatos e icono de la aplicación
cp ../build-macos-resources/Info.plist Echidnalink.app/Contents/
cp ../build-macos-resources/Info-helper.plist Echidnalink.app/Contents/Frameworks/'Electron Helper.app'/Contents/
cp ../../images/echidna.icns Echidnalink.app/Contents/Resources
