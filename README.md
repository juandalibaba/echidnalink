# EchidnaLink

la instalación de este software con `npm` o `yarn` requiere tener un entorno de desarrollo para compilar el paquete @serial.En el caso de MacOSX, se requiere tener instalado XCode. En esta URL se explica como hacerlo.

https://midu.dev/como-arreglar-no-xcode-or-clt-version-detected-npm-install/


# Instalación para el desarrollo

Clonar los repositorios:

```bash
# git clone https://gitlab.com/juandalibaba/echidnalink
# git clone https://gitlab.com/juandalibaba/echidnalink-lib
```

Instalar las dependencias:

```bash
cd echidnalink-lib
npm install
npm link
cd ..
cd echidnalink
npm install
npm link echidnalink-lib
npm rebuild
```

Para ejecutar la aplicación cambiamos al directorio `echidnalink` y hacemos:


```bash
# npm start
```

Si al iniciar da un problema con serialport:

```
# npm install serialport
# npm rebuild
```

# Construcción de ejecutables

En el directorio build_tools se ecuentran varias herramientas para construir ejecutables para los sistemas operativos MacOS, Windows y Linux.

En dicho directorio se ecuentra el subdirectorio `electron-prebuild` con los archivos (comprimidos) precompilados de electron para los 3 sistemas operativos. Basta descomprimirlos para tener el ejecutable base de electron listo para ejecutarse. A este ejecutable hay que añadirle los recursos HTML/CSS/javascript/imágenes/etc, necesarios para dar forma a la aplicación que se desarrolla (ver documentación de electron para entender como se construyen aplicaciones desktop con electron).

Las herramientas `build-macos.sh`, `build-windows.sh` y `build-linux.sh` utilizan las versiones precompiladas anteriores para "meter" en 
ellas la aplicación echidna-link empaquetada en un fichero asar y modificar algunos archivos para personalizar el ejecutable con las características de echidnalink (nombre e icono, fundamentalmente).

## MacOS

Ejecutar:

```
# ./build_macos.sh
```

Y se creará el directorio `electron-v16.0.4-darwin-x64` con un bundle de MacOS llamado `Echidnalink.app`. Este es el archivo ejecutable que hay que distribuir.

## Windows

Ejecutar:

```
# ./build_windows.sh
```

Y se creará el directorio `echidna-1.3.0-windows-x64` con un ejecutable de Windows llamado `echidna.exe`. Hay que distribuir TODO el directorio `echidna-1.3.0-windows-x64`, pues dentro, además del ejecutable `echidna.exe` van archivos necesarios como librerías DLL, el propio paquete asar etétera.

## Linux

Ejecutar:

```
# ./build_linux.sh
```

Y se creará el directorio `echidna-1.3.0-linux-x64` con un ejecutable de Linux llamado `echidnalink`. Hay que distribuir el directorio completo.

## Limpieza del directorio `build_tools`

Lanzando el script `clean_dir.sh` se borran todos los directorios que se han creado al lanzar las utilidades de creación de ejecutables y que NO DEBEN subirse al repositorio.


# Creación de ejecutables e instalables con electron-builder

En este apartado explicamos una forma alternativa de crear los instalables mediante el uso de una herramienta que automatiza el procesos de construcción de ejecutables y que, además, es capaz de construir instalables para las tres plataformas: MacOS, Windows y Linux. 

NOTA: Hemos de aclarar que, aunque la construcción de ejecutables mediante los scripts `buil_tools/build-PLATFORM.sh` se hace también de forma automática, dichos scripts han sido desarrollados por mí mismo basándome en el procedimiento manual que se explica en https://www.electronjs.org/docs/latest/tutorial/application-distribution#manual-distribution. Creo que es importante mantener estos scripts, aunque en la práctica sea más idoneo usar `electron-builder`, pues de esa manera se adquiere el conocimiento básico sobre la construcción de ejecutables con electron y, en última instancia, no dependemos exclusivamente de la herramienta `electron-builder`.

Para usar `electron-builder` se recomienda utilizar `yarn` como herramienta de gestión de dependencias.

Para construir ejecutables/instalables, hay que ejecutar los siguientes comandos desde 
cada plataforma. Es decir, para construir los ejecutables de Linux, hay que lanzarlos desde una plataforma Linux, y lo mismo para los demás.

```bash
# yarn
# yarn dist
```

En Linux se crea un directorio `dist` en cuyo interior se encuentra:

- Una aplicación autocontendida de tipo AppImage. Es muy útil para distribuirla, pues se trata de un solo archivo que puede ejecutarse directamente desde el navegador de archivos o desde una terminal.

- Un paquete debian

- Un directorio con el ejecutable y todas las dependencias necesarias para la ejecución de echidnalink, es decir, lo mismo que se obtienen con el script `build_tools/build-linux.sh`.


## API

Output and requested input values

socket.emit('event_name', option_object);

event name        option object

led         { name: <name>, action: action }                   | name = 'yellow' | 'green' | 'red'
                                                               | action = 'on' | 'off'
rgb         { action: 'color', color: <color_object>}          | color_object = {red: 10, green: 20, blue: 56}
                                                               |
rgb         { action: action }                                 | action = 'on'| 'off'
                                                               |
io          { id: <id_io>, action: 'write', value: <value>}    | id_io = 'io1' | 'io2' | 'io3'
                                                               | value = 1 | 0
io          { id: <id_io>, action: 'read' }                    | id_io = 'io1' | 'io2' | 'io3'
                                                               |
piezo       {                                                  |                
                action: 'frequency',                           |
                frequency: <frequency>,                        | frequency is the frequency in Hz
                duration: <duration>                           | duration is the duration in ms
            }                                                  |
piezo       {                                                  |
                action: 'play',                                | play a song
                tune: <tune>,                                  | tune is an array as mario_intro (see below)
                callback: <callback>                           | callback is a function to be executed when song is played,
            }                                                  | callback can be null

joystick    { action: 'read' }

accelerometer {action: 'read' }

ldr          { action: 'read' }

analog_in    { action: 'read' }

-----------
Input events

submit.on(event, data => {
    // code to deal with data
})

where event can be:

- 'button'

const mario_intro = {
    song: [
      ["E5", 1/4],
      [null, 1/4],
      ["E5", 1/4],
      [null, 3/4],
      ["E5", 1/4],
      [null, 3/4],
      ["C5", 1/4],
      [null, 1/4],
      ["E5", 1/4],
      [null, 3/4],
      ["G5", 1/4],
      [null, 7/4],
      ["G4", 1/4],
      [null, 7/4]
    ],
    tempo: 200
  };



piezo       { action: 'frequency', frequency: 440, duration: 1000}
## To Use

To clone and run this repository you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# Clone this repository
git clone https://github.com/electron/electron-quick-start
# Go into the repository
cd electron-quick-start
# Install dependencies
npm install
# Run the app
npm start
```

Note: If you're using Linux Bash for Windows, [see this guide](https://www.howtogeek.com/261575/how-to-run-graphical-linux-desktop-applications-from-windows-10s-bash-shell/) or use `node` from the command prompt.

## Resources for Learning Electron

- [electronjs.org/docs](https://electronjs.org/docs) - all of Electron's documentation
- [electronjs.org/community#boilerplates](https://electronjs.org/community#boilerplates) - sample starter apps created by the community
- [electron/electron-quick-start](https://github.com/electron/electron-quick-start) - a very basic starter Electron app
- [electron/simple-samples](https://github.com/electron/simple-samples) - small applications with ideas for taking them further
- [electron/electron-api-demos](https://github.com/electron/electron-api-demos) - an Electron app that teaches you how to use Electron
- [hokein/electron-sample-apps](https://github.com/hokein/electron-sample-apps) - small demo apps for the various Electron APIs
